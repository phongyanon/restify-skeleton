const config = require('config');

const restify = require('restify');
const mongoose = require('mongoose');
const restifyPlugins = require('restify-plugins');
const server = restify.createServer();

// middle ware
server.use(restifyPlugins.jsonBodyParser({ mapParams: true }));
server.use(restifyPlugins.acceptParser(server.acceptable));
server.use(restifyPlugins.queryParser({ mapParams: true }));
server.use(restifyPlugins.fullResponse());

/**
  * Start Server, Connect to DB & Require Routes
  */
 server.listen(config.get('port'), () => {
	// establish connection to mongodb
	mongoose.Promise = global.Promise;
	mongoose.connect(config.get('db_uri') + config.get('db_name'), { useNewUrlParser: true });

	const db = mongoose.connection;

	db.on('error', (err) => {
	    console.error(err);
	    process.exit(1);
	});

	db.once('open', () => {
		require('./api/todo/routes')(server);
		require('./api/user/routes')(server);
	    console.log(`Server is listening on port ${config.port}`);
	});
});

module.exports = server; // export for test

