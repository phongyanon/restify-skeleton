
process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const should = chai.should();
const expect = chai.expect;
const config = require('config');

const mongoose = require("mongoose");
const User = require('./models');

chai.use(chaiHttp);

//Our parent block
describe('User', () => {
    beforeEach((done) => { //Before each test we empty the database
        User.deleteMany({}, (err) => { 
           done();           
        });        
    });
/*
  * Test the /GET route
  */
  describe('/GET User', () => {
      it('it should get no User', (done) => {
        chai.request(server)
            .get('/getAllUser')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.result.length.should.be.eql(0);
            done();
            });
      });
  });

  /*
  * Test the /POST route
  */
    describe('/POST User', () => {
        it('it POST a User', (done) => {
            let user = {
                name: 'Pun',
                username: 'sikorn',
                password: '1234',
                point: '10'
            }
        chai.request(server)
            .post('/addUser')
            .send(user)
            .end((err, res) => {
                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property('error');
                res.body.should.have.property('result').eql('successfully created');
            done();
            });
        });
        it('it POST a User with iteration username', (done) => {
            let user = new User({
                name: 'Pon',
                username: 'sikorn',
                password: '5678',
                point: '11'
            });
            user.save((err, user) => {
                chai.request(server)
                .post('/addUser')
                .send(user)
                .end((err, res) => {
                    expect(res).to.have.status(405);
                done();
                });
            });
        });
    });

    /*
    * Test the /GET/:id route
    */
    describe('/GET User', () => {
        it('it should GET a User by the given id', (done) => {
            let data = new User({
                name: 'Cherprang',
                username: 'cherprang',
                password: '5678',
                point: 20
            });
            data.save((err, user) => {
                chai.request(server)
                .get('/getUser')
                .query({user_id: user.id})
                .end((err, res) => {


                    let body = res.body;
                    res.should.have.status(200);
                    body.should.be.a('object');
                    body.should.have.property('error').eql(false);
                    body.should.have.property('result');
                    body.result.should.have.property('name').eql('Cherprang');
                    body.result.should.have.property('username').eql('cherprang');
                    body.result.should.have.property('point').eql(20);
                done();
                });
            });
        });
    });

     /*
      * Test the /PUT/:id route
      */
    describe('/PUT User', () => {
        it('it should UPDATE a User given the id', (done) => {
            let data = new User({
                name: 'Jenish',
                username: 'jenish',
                password: '1234',
                point: 15
            });
            data.save((err, user) => {
                  chai.request(server)
                  .put('/updateUser')
                  .send({user_id: user.id, update: {point: 30} })
                  .end((err, res) => {
    

                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('error').eql(false);
                        res.body.should.have.property('result').eql('succussfully updated');
                    done();
                  });
            });
        });
    });

     /*
      * Test the /DELETE/:id route
      */
    describe('/DELETE User', () => {
        it('it should DELETE a User given the id', (done) => {
            let data = new User({
                name: 'Jan',
                username: 'jan',
                password: '1234',
                point: 10
            });
            data.save((err, user) => {
                  chai.request(server)
                  .delete('/deleteUser')
                  .send({user_id: user.id})
                  .end((err, res) => {
    

                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.result.should.have.property('ok').eql(1);
                        res.body.result.should.have.property('n').eql(1);
                    done();
                  });
            });
        });
    });

    // Test login 
    describe('/GET login user', () => {
        it('It should not successfully login with wrong password', (done) => {
            let user_data = new User({
                name: 'Mobile',
                username: 'mobile',
                password: '$2y$10$NnhzzEpRTV/twqczy7Y3muNsi9fMI5BrY3XLVqUTS9kAObgDbV7d2',
                point: 20
            });
            user_data.save((err, user) => {
                chai.request(server)
                .get('/login')
                .query({username: 'mobile', password: '5678'})
                .end((err, res) => {

                    expect(res).to.have.status(200);

                    res.body.should.have.property('result').eql('wrong username or password');
                    res.body.should.have.property('error').eql(false);

                    done();
                })
            });

        });

        it('It should successfully login with correct password', (done) => {
            let user_data = new User({
                name: 'Mobile',
                username: 'mobile',
                password: '$2b$12$0kuQO.y2vcZOml0AIHkXOeNI4UtFkorhsObXwpLs3qlJvqMk843KS',
                point: 20
            });
            user_data.save((err, user) => {
                chai.request(server)
                .get('/login')
                .query({username: 'mobile', password: '1234'})
                .end((err, res) => {
                    expect(res).to.have.status(200);
    
                    res.body.should.have.property('result').eql('successfully login');
                    res.body.should.have.property('error').eql(false);
    
                    done();
                })
            });
        });
    });

    // Test change password with wrong password
    describe('/PUT Test change password', () => {
        it('it should not change password with wrong confirmed password', (done) => {
            let user_data = new User({
                name: 'Orn',
                username: 'orn',
                password: '$2b$12$0kuQO.y2vcZOml0AIHkXOeNI4UtFkorhsObXwpLs3qlJvqMk843KS',
                point: 20
            });
            user_data.save((err, user) => {  
                chai.request(server)
                .put('/changePassword')
                .send({user_id: user.id, password: '12345', update: {new_password: '5678'}})
                .end((err, res) => {

                    expect(res).to.have.status(405);

                    done();
                })
            }); 
        });
        it('it should successfully change password', (done) => {
            let user_data = new User({
                name: 'Music',
                username: 'music',
                password: '$2b$12$0kuQO.y2vcZOml0AIHkXOeNI4UtFkorhsObXwpLs3qlJvqMk843KS',
                point: 20
            });
            user_data.save((err, user) => {  
                chai.request(server)
                .put('/changePassword')
                .send({user_id: user.id, password: '1234', update: {new_password: '5678'}})
                .end((err, res) => {

                    expect(res).to.have.status(200);

                    res.body.should.have.property('result').eql('successfully change password');
                    res.body.should.have.property('error').eql(false);

                    done();
                })
            });            
        });
    });

});
