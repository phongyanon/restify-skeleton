module.exports.getInstance = getInstance

var events = require('events');

function getInstance(params={}) {

    var store = function(params) {
        switch (params.type) {
            case 'mongo' :
                    var StoreEngine = require('./mongo');  
                    this.uri = params.db_uri;
                    this.db = params.db_name;
                    this.client = new StoreEngine.create({
                        db_uri: this.uri, 
                        db_name: this.db,
                    });
                    break;
        }
    }

    store.prototype = new events.EventEmitter;

    store.prototype.addUser = function(ctx) {
        return new Promise( async (resolve, reject) => {
            try {
                let check_result = await this.client.getUserByUsername({'username': ctx.username});
                if (check_result === null) {
                    let result = await this.client.addUser(ctx);                
                    resolve(result);  
                } else {
                    resolve(false);
                }
            } catch (err) {
                reject('addUser ==> ' +  err.toString());
            }
        });
    }

    store.prototype.getUser = function(ctx){
        return new Promise( async (resolve, reject) => {
            try {
                let result = await this.client.getUser({'user_id': ctx.user_id});            
                resolve(result);
            } catch (err) {
                reject('getUser ==> ' +  err.toString());
            }
        });
    };

    store.prototype.getAllUser = function(){
        return new Promise( async (resolve, reject) => {
            try {
                let result = await this.client.getAllUser();                
                resolve(result);
            } catch (err) {
                reject('getAllUser ==> ' +  err.toString());
            }
        });        
    };

    store.prototype.login = function(ctx){
        return new Promise( async (resolve, reject) => {
            try {
                let user = await this.client.getUserByUsername({'username': ctx.username});
                let check_pw = await this.client.checkPassword({
                        'your_password': ctx.password,
                        'password': user.password
                    });
                if (check_pw === true){
                    resolve('successfully login');
                } else {
                    resolve('wrong username or password')
                }
            } catch (err) {
                reject('login ==> ' +  err.toString());
            }
        });
    };
    store.prototype.changePassword = function(ctx){
        return new Promise( async (resolve, reject) => {
            try {
                let user = await this.client.getUser({'user_id': ctx.user_id});
                let check_pw = await this.client.checkPassword({
                        'your_password': ctx.password,
                        'password': user.password
                    });
                if (check_pw === true){
                    let update_result = await this.client.changePassword({
                        'user_id': ctx.user_id, 'update': ctx.update
                    });
                    if (update_result === true){
                        resolve('successfully change password');
                    } else {
                        reject('invalid change password');
                    }
                } else {
                    resolve(false);
                }
            } catch (err) {
                reject('changePassword ==> ' +  err.toString());
            }
        });
    };

    store.prototype.updateUser = function(ctx){
        return new Promise( async (resolve, reject) => {
            try {
                let result = await this.client.updateUser({'user_id': ctx.user_id});                
                resolve(result);
            } catch (err) {
                reject('updateUser ==> ' +  err.toString());
            }
        });  
    };

    store.prototype.deleteUser = function(ctx){
        return new Promise( async (resolve, reject) => {
            try {
                let result = await this.client.deleteUser({'user_id': ctx.user_id});                
                resolve(result);
            } catch (err) {
                reject('deleteUser ==> ' +  err.toString());
            }
        }); 
    };

    return new store(params);
}