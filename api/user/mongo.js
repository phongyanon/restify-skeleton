module.exports.create = create

const errors = require('restify-errors');
const User = require('./models');

const events = require('events');
const bcrypt = require('bcrypt');
const saltRounds = 12;

function create(params = {}){

    function store(params){
        // this.client = require('mongodb').MongoClient;
        this.db_uri = params.db_uri;
        this.db_name = params.db_name;
        console.log(this.db_uri, this.db_name);
    }

    store.prototype =  new events.EventEmitter;

    store.prototype.addUser = function(ctx){
        return new Promise( (resolve, reject) => {
            let salt = bcrypt.genSaltSync(saltRounds);
            let hash = bcrypt.hashSync(ctx.password, salt); // encode password
            ctx.password = hash;

            let user_obj = new User(ctx);
            user_obj.save(function(err, obj) {
                if (err) reject(new errors.InternalError(err.message));
                resolve('successfully created');
            });
        });
    };

    store.prototype.getAllUser = function(ctx){
        return new Promise( (resolve, reject) => {
            User.apiQuery(ctx, function(err, docs) {
                if (err) reject(new errors.InvalidContentError(err.errors.name.message));
                resolve(docs);
            });
        });
    };

    store.prototype.getUser = function(ctx){
        return new Promise( (resolve, reject) => {
            User.findOne({'_id': ctx.user_id}, function(err, docs) {
                if (err) reject(err.toString());
                resolve(docs);
            });
        });
    };

    store.prototype.getUserByUsername = function(ctx){
        return new Promise( (resolve, reject) => {
            User.findOne(ctx, function(err, docs) {
                if (err) reject(err.toString());
                if (docs !== null){
                    resolve({'username': docs.username, 'password': docs.password});
                } else {
                    resolve(null);
                }
            });
        });
    };

    store.prototype.updateUser = function(ctx){
        return new Promise( (resolve, reject) => {
            User.findOne({ _id: ctx.user_id }, function(err, doc) {
                if (err) reject(new errors.InvalidContentError(err.errors.name.message)); 
    
                User.updateOne({ _id: ctx.user_id }, ctx.update , function(err) {
                    if (err) reject(new errors.InvalidContentError(err.errors.name.message)); 
                    resolve('succussfully updated');
                });
            });
        });
    };

    store.prototype.changePassword = function(ctx){
        return new Promise( (resolve, reject) => {
            User.findOne({ _id: ctx.user_id }, function(err, doc) {
                if (err) reject(new errors.InvalidContentError(err.errors.name.message)); 
    
                let salt = bcrypt.genSaltSync(saltRounds);
                let hash = bcrypt.hashSync(ctx.update.new_password, salt); // encode password
                ctx.update.new_password = hash;

                User.updateOne({ _id: doc._id }, ctx.update , function(err, result) {
                    if (err) reject(new errors.InvalidContentError(err.errors.name.message)); 
                    resolve(true);
                });
            });
        });   
    };

    store.prototype.checkPassword = function(ctx){
        return new Promise( async (resolve, reject) => {
            let result = bcrypt.compareSync(ctx.your_password, ctx.password);
            resolve(result);
        });
    };
    
    store.prototype.deleteUser = function(ctx){
        return new Promise( (resolve, reject) => {
            User.deleteOne({ _id: ctx.user_id }, function(err, result) {
                if (err) reject(new errors.InvalidContentError(err.errors.name.message));
                resolve(result);
            });
        });  
    };

    return new store(params);
}
