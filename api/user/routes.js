const config = require('config');
const DataStore = require('./store');

let ds = new DataStore.getInstance({
    'db_uri': config.get('db_uri'),
    'db_name': config.get('db_name'),
    'type': config.get('db_type')
});

// module.exports = {
//     addUser: async function(req, res){
//         try {
//             let result = ds.addUser(req.body);
//             res.send({'error': false, 'result': result});
//         } catch (err) {
//             res.send({'error': true, 'result': err.toString()});
//         }
//     }
// }

/**
 * Module Dependencies
 */
const errors = require('restify-errors');

/**
 * Model Schema
 */
const User = require('./models');

module.exports = function(server) {

	/**
	 * POST
	 */
	server.post('/addUser', async (req, res, next) => {
        try {
            if (!req.is('application/json')) {
                res.send({'error': true, 'result': new errors.InvalidContentError("Expects 'application/json'")})
            }
            let ctx = req.body || {};   
            let result = await ds.addUser(ctx);
            if (result === false){
                res.send(405);
            } else {
                res.send(201, {'error': false, 'result': result});
            }
        } catch (err) {
            res.send(405, {'error': true, 'result': err.toString()});
        }
        next();
	});

	/**
	 * LIST
	 */
	server.get('/getUser', async (req, res, next) => {
        try {
            let ctx = req.query || {};
            let result = await ds.getUser(ctx);
            res.send({'error': false, 'result': result});            
        } catch (err) {
            res.send({'error': true, 'result': err.toString()});            
        }
        next();
	});

	/**
	 * GET
	 */
	server.get('/getAllUser', async (req, res, next) => {
        try {
            let result = await ds.getAllUser();
            res.send(200, {'error': false, 'result': result});            
        } catch (err) {
            res.send({'error': true, 'result': err.toString()});            
        }
        next();
	});

	/**
	 * UPDATE
	 */
	server.put('/updateUser', async (req, res, next) => {
        try {
            if (!req.is('application/json')) {
                res.send({'error': true, 'result': new errors.InvalidContentError("Expects 'application/json'")})
            }
            let ctx = req.body || {};   
            let result = await ds.updateUser(ctx);
            res.send({'error': false, 'result': result});
        } catch (err) {
            res.send({'error': true, 'result': err.toString()});
        }
        next();
	});

	/**
	 * DELETE
	 */
	server.del('/deleteUser', async (req, res, next) => {
        try {
            if (!req.is('application/json')) {
                res.send({'error': true, 'result': new errors.InvalidContentError("Expects 'application/json'")})
            }
            let ctx = req.body || {};
            if (ctx !== {}){
                let result = await ds.deleteUser(ctx);
                res.send({'error': false, 'result': result});
            } else {
                res.send({'error': false, 'result': 'nothing to be deleted'});
            }
        } catch (err) {
            res.send({'error': true, 'result': err.toString()});
        }
        next();
    });
    
    // login
    server.get('/login', async (req, res, next) => {
        try {
            let ctx = req.query || {};
            let result = await ds.login(ctx);
            res.send({'error': false, 'result': result});            
        } catch (err) {
            res.send({'error': true, 'result': err.toString()});            
        }
        next();        
    });

    // change password
    server.put('/changePassword', async (req, res, next) => {
        try {
            if (!req.is('application/json')) {
                res.send({'error': true, 'result': new errors.InvalidContentError("Expects 'application/json'")})
            }
            let ctx = req.body || {};   
            let result = await ds.changePassword(ctx);
            if (result === false){
                res.send(405);
            } else {
                res.send({'error': false, 'result': result});
            }
        } catch (err) {
            res.send({'error': true, 'result': err.toString()});
        }
        next();
    });
};