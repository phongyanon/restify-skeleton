process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../../server');
const should = chai.should();
const expect = chai.expect;
const config = require('config');

const mongoose = require("mongoose");
const Todo = require('./models');

chai.use(chaiHttp);

//Our parent block
describe('Todo', () => {
    beforeEach((done) => { //Before each test we empty the database
        Todo.deleteMany({}, (err) => { 
           done();           
        });        
    });
/*
  * Test the /GET route
  */
  describe('/GET Todo', () => {
      it('it should GET all the todos', (done) => {
        chai.request(server)
            .get('/todos')
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('array');
                  res.body.length.should.be.eql(0);
              done();
            });
      });
  });

  /*
  * Test the /POST route
  */
    describe('/POST todos', () => {
        it('it POST a todos', (done) => {
            let task = {
                task: "AI trainning",
                status: "in progress"
            }
        chai.request(server)
            .post('/todos')
            .send(task)
            .end((err, res) => {
                res.should.have.status(201);
                res.body.should.be.a('object');
                res.body.should.have.property('error');
                res.body.should.have.property('result').eql('successfully created.');
            done();
            });
        });
    });

    /*
    * Test the /GET/:id route
    */
    describe('/GET/:id todos', () => {
        it('it should GET a book by the given id', (done) => {
            let task = new Todo({task: "Full stack", status: "complete"});
            task.save((err, todo) => {
                chai.request(server)
                .get('/todos/' + todo.id)
                .send(task)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('status');
                    res.body.should.have.property('task');
                    res.body.should.have.property('_id').eql(todo.id);
                done();
                });
            });
        });
    });

     /*
      * Test the /PUT/:id route
      */
    describe('/PUT/:id todos', () => {
        it('it should UPDATE a todos given the id', (done) => {
            let task = new Todo({task: 'graphQL', status: 'in progress'});
            task.save((err, todo) => {
                  chai.request(server)
                  .put('/todos/' + todo.id)
                  .send({status: 'complete'})
                  .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('status').eql('complete');
                    done();
                  });
            });
        });
    });

     /*
      * Test the /DELETE/:id route
      */
    describe('/DELETE/:id todos', () => {
        it('it should DELETE a todos given the id', (done) => {
            let task = new Todo({task: 'graphQL', status: 'in progress'});
            task.save((err, todo) => {
                  chai.request(server)
                  .delete('/todos/' + todo.id)
                  .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.result.should.have.property('ok').eql(1);
                        res.body.result.should.have.property('n').eql(1);
                    done();
                  });
            });
        });
    });

});
