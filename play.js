const bcrypt = require('bcrypt')
const saltRounds = 12;

let ctx = {
    password: '1234',
    your_password: '$2y$12$E8fIGuLmt3DLvdM4LQfWw.lKYnr/rQmvujkQPCtknK.YdtyOr5iu.'
}

let salt = bcrypt.genSaltSync(saltRounds);
let hash = bcrypt.hashSync('1234', salt); // encode password
console.log(hash)

let result = bcrypt.compareSync('1234', '$2b$12$0kuQO.y2vcZOml0AIHkXOeNI4UtFkorhsObXwpLs3qlJvqMk843KS');
console.log(result);

// const events = require('events');
// const mongojs = require('mongojs');
// function create(params={}){

//     function store(params){
//         this.db = mongojs('testdb', ['User']);
//     }

//     store.prototype = new events.EventEmitter;

//     // create
//     store.prototype.addUser = function(ctx){
//         return new Promise( (resolve, reject) => {
//             this.db.User.insert(ctx, function(err, docs){
//                 if (err) reject(err.toString());
//                 resolve('sucessfully inserted');
//             });
//         });
//     }

//     // update
//     store.prototype.updateUser = function(ctx){
//         return new Promise( (resolve, reject) => {
//             this.db.User.update(ctx.query, ctx.update, function(err, docs){
//                 if (err) reject(err.toString());
//                 resolve('sucessfully updated');
//             });
//         });
//     }
//     store.prototype.changePassword = function(ctx){
//         return new Promise( (resolve, reject) => {
//             this.db.User.findAndModify({
//                 query: {username: ctx.username},
//                 update: {$set: {password: ctx.new_password}},
//                 new: false
//             }, function(err, doc, lastErrorObject){
//                 if (err) reject(err.toString());
//                 if (doc === undefined) reject('no document have been updated');
//                 // console.log(doc.password);
//                 resolve('sucessfully change password');
//             });
//         });
//     }

//     // read
//     store.prototype.getAllUser = function(){
//         return new Promise( (resolve, reject) => {
//             this.db.User.find({}, function(err, docs){
//                 if (err) reject(err.toString);
//                 resolve(docs);
//             });
//         });
//     }

//     store.prototype.getUser = function(ctx){
//         return new Promise( (resolve, reject) => {
//             this.db.User.find(ctx, function(err, docs){
//                 if (err) reject(err.toString);
//                 resolve(docs);
//             });
//         });       
//     }

//     // delete
//     store.prototype.deleteUser = function(ctx){
//         return new Promise( (resolve, reject) => {
//             this.db.User.remove(ctx, function(err, docs){
//                 if (err) reject(err.toString);
//                 resolve(docs);
//             });         
//         });
//     }
//     return new store(params);
// }

// async function removeAllUser(){
//     try {
//         let client = create();
//         let users = await client.getAllUser();
//         for (i in users){
//             let res = await client.deleteUser(users[i]);
//             console.log(res);
//         }   
//     } catch (err) {
//         throw(err);
//     }
// }

// async function runAll(){
//     try {
//         // removeAllUser();

//         let client = create();
//         let res1 = await client.getAllUser();
//         console.log(res1); 
//         let user1 = {'name': 'Kenchiro', 'username': 'Ken', 'password': '1234', 'role': 'fighter'}
//         let res2 = await client.addUser(user1);
//         console.log(res2);
//         let get_user = await client.getUser({'username': 'Ken'});
//         console.log(get_user);
//         let update_user = {
//             'query': {
//                 '_id': get_user[0]._id
//             },
//             'update': {
//                 'username': 'Goku',
//                 'password': '4567',
//                 'role': 'fighter'
//             }
//         }
//         let updated = await client.updateUser(update_user);
//         console.log(updated);
//         let change = await client.changePassword({'username': 'Goku', 'new_password': 'Vegita'});
//         console.log(change);
//         let res3 = await client.getUser({'username': 'Goku'});
//         console.log(res3);  
//     } catch (err) {
//         throw(err);
//     }
// }

// removeAllUser();
// runAll();
